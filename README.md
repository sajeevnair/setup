# Setup

This install most of the things you'll need to start except node

Currently We need node version to be v8 so after the setup finishes reload or open a new terminal to run 

nvm install node 8

if you have already installed node just use v8(untill that changes, there is an upcoming ui refactor so ..)

check using nvm run node --version

## Manual Steps

### NVM
If you have zshrc and bashrc and are using zhs/oh-my-zsh, nvm will only update bash so add these lines to zshrc:

	export NVM_DIR="$HOME/.nvm"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
	[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

### Add JAVA_HOME env

Sudo nano /etc/environment 

Add this line:

	JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
	

---
**NOTE**

You need to logout and login for the current user to be added to docker group. This is done so you don't have to sudo for docker. 

---