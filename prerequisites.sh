#!/bin/bash

# utils
sudo apt install --assume-yes curl

# Install packages to allow apt to use a repository over HTTPS:
sudo apt-get --assume-yes install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Add Docker�s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -


sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update

# docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# add user to docer group
sudo usermod -a -G docker $USER

#docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


#java 8
sudo apt install --assume-yes --fix-missing openjdk-8-jdk 

# maven
sudo apt install  --assume-yes maven

# yarn

sudo apt install  --assume-yes --no-install-recommends yarn 



#nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
exec bash

#node
nvm install v11